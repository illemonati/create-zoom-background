"use strict";
const canvas = document.querySelector(".mainCanvas");
const startButton = document.querySelector(".startButton");
const STRIPE_WIDTH = 1920 / 128;
const COLOR_0 = "#FF00FF";
const COLOR_1 = "#00FFFF";
const main = () => {
    startButton.addEventListener("click", startAndCapture);
};
const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));
const startAndCapture = async () => {
    drawFrame(0, STRIPE_WIDTH, COLOR_0, COLOR_1);
    //@ts-ignore
    const stream = canvas.captureStream(360);
    const recorder = startRecordAndDownload(stream);
    await startAnimation();
    recorder.stop();
};
const startRecordAndDownload = (stream) => {
    const recordedChunks = [];
    const options = { mimeType: "video/webm; codecs=vp9" };
    const recorder = new MediaRecorder(stream, options);
    const handleDataAvailable = (event) => {
        console.log("data-available");
        if (event.data.size > 0) {
            recordedChunks.push(event.data);
            download(recordedChunks);
        }
    };
    recorder.ondataavailable = handleDataAvailable;
    recorder.start();
    return recorder;
};
function download(recordedChunks) {
    const blob = new Blob(recordedChunks, {
        type: "video/webm",
    });
    const url = URL.createObjectURL(blob);
    const a = document.createElement("a");
    document.body.appendChild(a);
    a.style.display = "none";
    a.href = url;
    a.download = "test.webm";
    a.click();
    window.URL.revokeObjectURL(url);
}
const startAnimation = async () => {
    const canvasWidth = canvas.width;
    for (let i = 0; i < STRIPE_WIDTH * 4; i++) {
        drawFrame(i, STRIPE_WIDTH, COLOR_0, COLOR_1);
        await sleep(10);
    }
};
const drawFrame = (offset, stripeWidth, color0, color1) => {
    const canvasWidth = canvas.width;
    const canvasHeight = canvas.height;
    const ctx = canvas.getContext("2d");
    if (!ctx)
        return;
    for (let x = 0; x < canvasWidth; x += stripeWidth) {
        ctx.fillStyle = ((x / stripeWidth) & 1) == 0 ? color0 : color1;
        let trueX = x + offset;
        if (trueX > canvasWidth) {
            trueX = trueX - canvasWidth;
        }
        ctx.fillRect(trueX, 0, stripeWidth, canvasHeight);
        if (trueX + stripeWidth > canvasWidth) {
            ctx.fillRect(0, 0, trueX + stripeWidth - canvasWidth, canvasHeight);
        }
    }
};
main();
